﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PacketConnection
{
    /// <summary>
    /// The structure of the message sent and received.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct ViewStateStructure
    {
        /// <summary>
        /// The ID of the test object.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 24)]
        public string id;
        /// <summary>
        /// Used to indicate different boolean states of the SUT. Used to send information whether the test object is visible.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 24)]
        public string flags;

        /// <summary>
        /// The text attribute of the test object.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 512)]
        public string text;
        /// <summary>
        /// The position of the test object.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string position;
    }
    /// <summary>
    /// The abstract view state class, that should be overwritten. 
    /// </summary>
    public abstract class ViewState : IConnectionElement<ViewStateStructure>
    {
        const byte IS_VISIBLE_FLAG = 1;

        protected int m_nId;
        protected string m_sText, m_sPosition;
        private byte m_nFlags;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewStateStruct">The structure used to initialize the object.</param>
        public ViewState(ViewStateStructure viewStateStruct)
        {
            m_nId = int.Parse(viewStateStruct.id);
            m_nFlags = byte.Parse(viewStateStruct.flags);
            m_sText = viewStateStruct.text;
            m_sPosition = viewStateStruct.position;
        }

        public ViewState(int nId, byte nFlags, string sText, string sPosition)
        {
            m_nId = nId;
            m_nFlags = nFlags;
            m_sText = sText;
            m_sPosition = sPosition;
        }
        /// <summary>
        /// States whether the test object is visible.
        /// </summary>
        public bool IsVisible
        {
            set
            {
                m_nFlags = SetByte(m_nFlags, IS_VISIBLE_FLAG, value);
            }
            get
            {
                return (m_nFlags & IS_VISIBLE_FLAG) == 1;
            }
        }

        private byte SetByte(byte base_, byte targetFlag, bool value)
        {
            byte a = (byte)(base_ & ~targetFlag);
            byte b = (byte)(value ? targetFlag : (byte)0);
            return (byte)(a + b);
        }
        /// <summary>
        /// Used to get the corresponding structure of the object for communication.
        /// </summary>
        /// <returns>ViewStateStructure, which is filled based on the values of the object.</returns>
        public ViewStateStructure GetStructure()
        {
            ViewStateStructure structure = new ViewStateStructure();
            structure.id = m_nId.ToString();
            structure.flags = m_nFlags.ToString();
            structure.text = m_sText;
            structure.position = m_sPosition;
            return structure;
        }

    }
}
