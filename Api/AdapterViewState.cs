﻿using PacketConnection;

namespace UnityTesting
{
    /// <summary>
    /// View state, that provides information about the state of an objet in the SUT.
    /// </summary>
    public class AdapterViewState : ViewState
    {
        public AdapterViewState(ViewStateStructure viewStateStruct) : base(viewStateStruct)
        { }

        public AdapterViewState(int nId, string sText, string sPosition) : base(nId, 0, sText, sPosition)
        { }

        public AdapterViewState(int nId, string sText, bool bVisible) : base(nId, 0, sText, "")
        {
            IsVisible = bVisible;
        }
        /// <summary>
        /// Id of the object. Used to know, which object it is in the SUT.
        /// </summary>
        public int ID { get { return m_nId; } }
        /// <summary>
        /// The text attribute of the test object.
        /// </summary>
        public string Text {
            set { m_sText = value; }
            get { return m_sText; }
        }
    }
}
