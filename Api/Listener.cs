﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace PacketConnection
{
    public delegate void SocketAcceptedHandler(object sender, SocketAcceptedEventArgs e);

    public class SocketAcceptedEventArgs : EventArgs
    {
        public Socket Accepted
        {
            get;
            private set;
        }

        public IPAddress Address
        {
            get;
            private set;
        }

        public IPEndPoint EndPoint
        {
            get;
            private set;
        }

        public SocketAcceptedEventArgs(Socket sck)
        {
            Accepted = sck;
            Address = ((IPEndPoint)sck.RemoteEndPoint).Address;
            EndPoint = (IPEndPoint)sck.RemoteEndPoint;
        }
    }
    /// <summary>
    /// The listener used to set up the server.
    /// </summary>
    public class Listener
    {
        #region Variables
        private Socket m_socket = null;
        private bool m_bRunning = false;
        private int m_nPort = -1;
        #endregion

        #region Properties
        /// <summary>
        /// The socket used for connection.
        /// </summary>
        public Socket BaseSocket
        {
            get { return m_socket; }
        }
        /// <summary>
        /// Return whether the listener is running.
        /// </summary>
        public bool Running
        {
            get { return m_bRunning; }
        }
        /// <summary>
        /// Returns the port, which is being listened.
        /// </summary>
        public int Port
        {
            get { return m_nPort; }
        }
        #endregion

        public event SocketAcceptedHandler Accepted;

        public Listener()
        {

        }
        /// <summary>
        /// Starts listening to the specified port.
        /// </summary>
        /// <param name="port"></param>
        public void Start(int port)
        {
            if (m_bRunning)
                return;

            m_nPort = port;
            m_bRunning = true;
            m_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_socket.Bind(new IPEndPoint(IPAddress.Any, port));
            m_socket.Listen(100);
            m_socket.BeginAccept(acceptCallback, null);
        }
        /// <summary>
        /// Closes the socket connection.
        /// </summary>
        public void Stop()
        {
            if (!m_bRunning)
                return;

            m_bRunning = false;
            m_socket.Close();
        }

        private void acceptCallback(IAsyncResult ar)
        {
            try
            {
                Socket sck = m_socket.EndAccept(ar);

                if (Accepted != null)
                {
                    Accepted(this, new SocketAcceptedEventArgs(sck));
                }
            }
            catch
            {
            }

            if (m_bRunning)
                m_socket.BeginAccept(acceptCallback, null);
        }
    }
}
