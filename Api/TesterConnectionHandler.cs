﻿using System;
using System.Collections.Generic;
using PacketConnection;

namespace UnityTesting
{
    /// <summary>
    /// The connection handler, which establishes connection with the test manager at the program made with Unity.
    /// Handles sending and receveing message from the system under test (SUT).
    /// </summary>
    public class TesterConnectionHandler : ConnectionHandler
    {
        #region singleton
        private static volatile TesterConnectionHandler instance;
        private static object syncRoot = new Object();

        private const int SERVER_PORT_NR = 110;
        private const string HOST_NAME = "localhost";

        private const string TMP_FOLDER_PATH = "tmp\\mock\\";
        private TesterConnectionHandler() : base(TMP_FOLDER_PATH, SERVER_PORT_NR, "localhost") { }
        /// <summary>
        /// The Instance of the connection handler. One connection can be created.
        /// </summary>
        public static TesterConnectionHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new TesterConnectionHandler();
                    }
                }

                return instance;
            }
        }
        #endregion
        /// <summary>
        /// The delegate for view state events.
        /// </summary>
        /// <param name="lViewStates">View states received</param>
        public delegate void ViewStatesEvent(List<AdapterViewState> lViewStates);
        /// <summary>
        /// Triggered when view states are received from the SUT.
        /// The method SendGetViewState should be called to get the view states.
        /// </summary>
        public event ViewStatesEvent ViewStatesReceived;

        const string GET_VIEW_STATE = "get view state";
        const string TRIGGER_BUTTON = "trigger button";
        const string SET_LABEL_VALUE = "set label value";

        void ProcessVievStateReceived(byte[] aViewState)
        {
            List<AdapterViewState> lViewStates = UtilFunctions.ByteArrayToList<ViewStateStructure, AdapterViewState>(aViewState, viewStruct => new AdapterViewState(viewStruct));

            if (ViewStatesReceived != null)
                ViewStatesReceived(lViewStates);
        }

        protected override byte[] GetResponse(byte[] aReceivedMessage, out bool bDeleteFile)
        {
            byte[] aValueReceived;
            string sMessage = UtilFunctions.GetMessageAndValue(aReceivedMessage, out aValueReceived);
            Console.WriteLine("Received message: " + sMessage);

            switch (sMessage.Trim('\0'))
            {
                case GET_VIEW_STATE:
                    ProcessVievStateReceived(aValueReceived);
                    break;
            }
            bDeleteFile = true;
            return null;
        }
        /// <summary>
        /// Used to send a message to the SUT with a parameter, which is a view state.
        /// </summary>
        /// <param name="sMessage">Command, what the SUT should do. Can be "get view state", "trigger button", "set label value" or a custom message, which is also handled in the SUT.</param>
        /// <param name="newViewState"></param>
        public void SendMessageToUnity(string sMessage, AdapterViewState newViewState)
        {
            if (newViewState == null)
                SendMessage(sMessage);
            else
                SendMessage(sMessage, UtilFunctions.StructureToByteArray(newViewState.GetStructure()));
        }
        /// <summary>
        /// Sends a message to the Unity asking for the view state of the SUT. 
        /// </summary>
        public void SendGetViewState ()
        {
            SendMessage(GET_VIEW_STATE);
        }
        /// <summary>
        /// The method sends a message to the SUT to trigger the object with the same ID as the parameter's ID. This method can be used to trigger buttons in a menu or to click on objects in a game.
        /// </summary>
        /// <param name="newViewState">The object to be triggered.</param>
        public void SendTriggerButton(AdapterViewState newViewState)
        {
            SendMessageToUnity(TRIGGER_BUTTON, newViewState);
        }
        /// <summary>
        /// This methods send a message to the SUT to set a new value to an object in the game with the same ID as the parameter's ID. The new value to be set is the Text property of the parameter.
        /// </summary>
        /// <param name="newViewState">The object to be changed.</param>
        public void SendSetLabelValue(AdapterViewState newViewState)
        {
            SendMessageToUnity(SET_LABEL_VALUE, newViewState);
        }
        /// <summary>
        /// This method can be used to send any data to the SUT's connection handler. It is possible to write additional code in the SUT's connection handler, which is able to use the data. The frameworks' connection handler is able to send messages of any length. When the message is very large it splits the message up to smaller pieces and sends them one by one. 
        /// </summary>
        /// <param name="sCommand">Command, what the SUT should do.</param>
        /// <param name="aData">Byte array of data to send.</param>
        public void SendData (string sCommand, byte[] aData)
        {
            SendMessage(sCommand, aData);
        }
    }
}
