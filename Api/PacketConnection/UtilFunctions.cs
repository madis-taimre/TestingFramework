﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PacketConnection
{
    public static class UtilFunctions
    {
        const int MESSAGE_LENGTH = 48;

        public static byte[] StructureToByteArray(object str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        }

        public static T ByteArrayToStructure<T>(byte[] arr)
        {
            T str = default(T);
            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(arr, 0, ptr, size);
            str = (T)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);
            return str;
        }

        public delegate ListElement StructureToListElement<ListElement, StructureElement>(StructureElement structure);

        public static List<ListElement> ByteArrayToList<StructureElement, ListElement>
            (byte[] aData, StructureToListElement<ListElement, StructureElement> structToListElem)
        {
            List<ListElement> lListElements = new List<ListElement>();
            int nSizeOfStructure = GetStructureSize<StructureElement>();
            int nNrOfStructures = aData.Length / nSizeOfStructure;
            byte[] aStructureInBytes = new byte[nSizeOfStructure];
            for (int i = 0; i < nNrOfStructures; i++)
            {
                Buffer.BlockCopy(aData, nSizeOfStructure * i, aStructureInBytes, 0, nSizeOfStructure);
                StructureElement structure = UtilFunctions.ByteArrayToStructure<StructureElement>(aStructureInBytes);
                lListElements.Add(structToListElem(structure));
            }
            return lListElements;
        }

        public static int GetStructureSize<T>()
        {
            return Marshal.SizeOf(default(T));
        }

        public static byte[] GetBytesToSend(string sMessage, byte[] aValue)
        {
            byte[] aMessageInBytes = Encoding.ASCII.GetBytes(sMessage);

            byte[] aByteToSend = new byte[MESSAGE_LENGTH + aValue.Length];
            Buffer.BlockCopy(aMessageInBytes, 0, aByteToSend, 0, aMessageInBytes.Length);
            Buffer.BlockCopy(aValue, 0, aByteToSend, MESSAGE_LENGTH, aValue.Length);
            return aByteToSend;
        }

        public static string GetMessageAndValue(byte[] aDataReceived, out byte[] aValue)
        {
            string sMessage = Encoding.ASCII.GetString(aDataReceived.Take(MESSAGE_LENGTH).ToArray());
            aValue = aDataReceived.Skip(MESSAGE_LENGTH).ToArray();
            return sMessage;
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
