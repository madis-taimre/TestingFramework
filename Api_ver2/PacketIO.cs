﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PacketConnection
{
    /// <summary>
    /// Used to write the message into a packet.
    /// </summary>
    public class PacketWriter : BinaryWriter
    {

        private MemoryStream _ms;
        private BinaryFormatter _bf;

        public PacketWriter()
            : base()
        {
            _ms = new MemoryStream();
            _bf = new BinaryFormatter();
            OutStream = _ms;
        }
        /// <summary>
        /// Serializes the object to the binary formatter.
        /// </summary>
        /// <param name="obj"></param>
        public void WriteT(object obj)
        {
            _bf.Serialize(_ms, obj);
        }
        /// <summary>
        /// Returns the bytes of the memory stream.
        /// </summary>
        /// <returns></returns>
        public byte[] GetBytes()
        {
            Close();

            byte[] data = _ms.ToArray();

            return data;
        }
    }

    public class PacketReader : BinaryReader
    {
        private BinaryFormatter _bf;
        public PacketReader(byte[] data)
            : base(new MemoryStream(data))
        {
            _bf = new BinaryFormatter();
        }
        /// <summary>
        /// Desearlizes the binary formatter.
        /// </summary>
        /// <typeparam name="T">The type of the object.</typeparam>
        /// <returns>The requested deserialized object.</returns>
        public T ReadObject<T>()
        {
            return (T)_bf.Deserialize(BaseStream);
        }
    }
}
