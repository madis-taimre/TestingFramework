﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PacketConnection
{
    /// <summary>
    /// The structure of the message sent and received.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct ObjectStateStructure
    {
        /// <summary>
        /// The ID of the test object.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string name;
        /// <summary>
        /// Used to indicate different boolean states of the SUT. Used to send information whether the test object is visible.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 24)]
        public string flags;

        /// <summary>
        /// The text attribute of the test object.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 512)]
        public string text;
        /// <summary>
        /// The position of the test object.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string position;
        /// <summary>
        /// The animation currently running on the object.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string animation;
        /// <summary>
        /// The value of the slider of the test object.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string numericValue;
    }
    /// <summary>
    /// The abstract view state class, that should be overwritten. 
    /// </summary>
    public abstract class ObjectState : IConnectionElement<ObjectStateStructure>
    {
        const byte IS_VISIBLE_FLAG = 1;

        protected string m_sName;
        protected string m_sText, m_sPosition, m_sAnimation;
        protected float m_fNumericValue;
        private byte m_nFlags;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewStateStruct">The structure used to initialize the object.</param>
        public ObjectState(ObjectStateStructure objectStateStruct)
        {
            m_sName = objectStateStruct.name;
            m_nFlags = byte.Parse(objectStateStruct.flags);
            m_sText = objectStateStruct.text;
            m_sPosition = objectStateStruct.position;
            m_sAnimation = objectStateStruct.animation;
            m_fNumericValue = int.Parse(objectStateStruct.numericValue);
        }

        public ObjectState(string sName, byte nFlags, string sText, string sPosition, string sAnimation, float fNumericValue)
        {
            m_sName = sName;
            m_nFlags = nFlags;
            m_sText = sText;
            m_sPosition = sPosition;
            m_sAnimation = sAnimation;
            m_fNumericValue = fNumericValue;
        }
        /// <summary>
        /// States whether the test object is visible.
        /// </summary>
        public bool IsVisible
        {
            set
            {
                m_nFlags = SetByte(m_nFlags, IS_VISIBLE_FLAG, value);
            }
            get
            {
                return (m_nFlags & IS_VISIBLE_FLAG) == 1;
            }
        }

        private byte SetByte(byte base_, byte targetFlag, bool value)
        {
            byte a = (byte)(base_ & ~targetFlag);
            byte b = (byte)(value ? targetFlag : (byte)0);
            return (byte)(a + b);
        }
        /// <summary>
        /// Used to get the corresponding structure of the object for communication.
        /// </summary>
        /// <returns>ViewStateStructure, which is filled based on the values of the object.</returns>
        public ObjectStateStructure GetStructure()
        {
            ObjectStateStructure structure = new ObjectStateStructure();
            structure.name = m_sName;
            structure.flags = m_nFlags.ToString();
            structure.text = m_sText;
            structure.position = m_sPosition;
            structure.animation = m_sAnimation;
            structure.numericValue = m_fNumericValue.ToString();
            return structure;
        }

    }
}
