﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace PacketConnection
{
    public enum QueueType : byte
    {
        Download,
        Upload
    }

    public delegate void DebugEventHandler(string message);

    public class TransferQueue
    {
        public static event DebugEventHandler DebugEvent;

        public static Random Rand = new Random();

        public static TransferQueue CreateUploadQueue(TransferClient client, string filename)
        {
            var queue = new TransferQueue();
            queue.Filename = Path.GetFileName(filename);
            queue.Client = client;
            queue.Type = QueueType.Upload;
            queue.FS = new FileStream(filename, FileMode.Open);

            //Console.WriteLine("Upload file name: " + filename);
            queue.Thread = new Thread(new ParameterizedThreadStart(transferProc));
            queue.Thread.IsBackground = true;
            queue.ID = Rand.Next();
            Console.WriteLine("Upload queue id " + queue.ID);
            queue.Length = queue.FS.Length;

            return queue;
        }

        public static TransferQueue CreateDownloadQueue(TransferClient client, int id, string saveName, long length)
        {
            var queue = new TransferQueue();
            queue.Filename = Path.GetFileName(saveName);
            queue.Client = client;
            queue.Type = QueueType.Download;

            Console.WriteLine("Download file name: " + saveName);
            queue.FS = new FileStream(saveName, FileMode.OpenOrCreate);
            queue.FS.SetLength(length);
            queue.Length = length;
            queue.ID = id;
            Console.WriteLine("Download queue id " + queue.ID);
            return queue;
        }

        const int FILE_BUFFER_SIZE = 8175;
        static byte[] fileBuffer = new byte[FILE_BUFFER_SIZE];

        ManualResetEvent pauseEvent;

        public int ID;
        public int Progress, LastProgress;

        public long Transferred;
        public long Index;
        public long Length;

        public bool Running;
        public bool Paused;


        public string Filename;

        public QueueType Type;

        public TransferClient Client;
        public Thread Thread;
        public FileStream FS;

        TransferQueue()
        {
            pauseEvent = new ManualResetEvent(true);
            Running = true;
        }

        public void Start()
        {
            Running = true;
            Thread.Start(this);
        }

        public void Stop()
        {
            Running = false;
        }

        public void Pause()
        {
            if (!Paused)
                pauseEvent.Reset();
            else
                pauseEvent.Set();

            Paused = !Paused;
        }

        public void Close()
        {
            try
            {
                Client.Transfers.Remove(ID);
            }
            catch { }

            Running = false;
            FS.Close();
            //pauseEvent.Dispose();
            pauseEvent.Close();
            Client = null;
        }

        public void Write(byte[] bytes, long index)
        {
            lock (this)
            {
                FS.Position = index;
                FS.Write(bytes, 0, bytes.Length);
                Transferred += bytes.Length;
            }
        }

        static void transferProc(object o)
        {
            TransferQueue queue = (TransferQueue)o;

            while (queue.Running && queue.Index < queue.Length)
            {
                queue.pauseEvent.WaitOne();

                if (!queue.Running)
                    break;

                lock (fileBuffer)
                {
                    queue.FS.Position = queue.Index;

                    int read = queue.FS.Read(fileBuffer, 0, fileBuffer.Length);

                    PacketWriter pw = new PacketWriter();
                    pw.Write((byte)Headers.Chunk);
                    pw.Write(queue.ID);
                    pw.Write(queue.Index);
                    pw.Write(read);
                    pw.Write(fileBuffer, 0, read);

                    queue.Transferred += read;
                    queue.Index += read;

                    queue.Client.Send(pw.GetBytes());

                    queue.Progress = (int)((queue.Transferred * 100) / queue.Length);

                    if (queue.LastProgress < queue.Progress)
                    {
                        queue.LastProgress = queue.Progress;

                        queue.Client.callProgressChanged(queue);

                    }

                    Thread.Sleep(1);
                }
            }

            queue.Close();
        }
    }
}
