﻿using System;
using PacketConnection;
using System.IO;
using System.Threading;

namespace PacketConnection
{
    public abstract class ConnectionHandler
    {
        public delegate void ConnectionHandlerEvent(ConnectionHandler handler);

        private readonly string m_sTempFolderPath;
        private int m_nPort;
        private bool m_bConnected = false;
        public event DebugEventHandler DebugEvent;
        public event ConnectionHandlerEvent OnConnected, OnDisconnected;

        Listener listener;

        protected TransferClient m_transferClient = null;
        string m_sHost;

        volatile bool m_bCancelConnect = false;
        protected ConnectionHandler(string sTempFolderPath, int nPort, string sHost)
        {
            m_sTempFolderPath = sTempFolderPath;
            if (!Directory.Exists(m_sTempFolderPath))
                Directory.CreateDirectory(m_sTempFolderPath);
            m_nPort = nPort;
            m_sHost = sHost;
            SetupListener();
        }

        private void SetupEvents()
        {
            m_transferClient.Complete += TransferClient_Complete;
            m_transferClient.Queued += TransferClient_Queued;
            m_transferClient.Disconnected += TransferClient_Disconnected;
        }

        private void DeregisterEvents()
        {
            if (m_transferClient == null)
                return;
            m_transferClient.Complete -= TransferClient_Complete;
            m_transferClient.Disconnected -= TransferClient_Disconnected;
            m_transferClient.Queued -= TransferClient_Queued;
        }

        private void TransferClient_Queued(object sender, TransferQueue queue)
        {
            if (queue.Type == QueueType.Download)
            {
                if (DebugEvent != null)
                    DebugEvent("At starting download");
                Console.WriteLine("At starting download");
                m_transferClient.StartTransfer(queue);
            }
            else
            {
                if (DebugEvent != null)
                    DebugEvent("At starting upload " + queue.Filename);
                Console.WriteLine("At starting upload " + queue.Filename);
                //queue.Start();
            }
        }

        public bool IsConnected()
        {
            return m_transferClient != null && m_transferClient.Connected != false && m_bConnected != false;
        }

        private void TransferClient_Complete(object sender, TransferQueue queue)
        {
            byte[] sentMessage = File.ReadAllBytes(m_sTempFolderPath + queue.Filename);
            byte[] aRetMessage = GetResponse(sentMessage);
            if (aRetMessage != null)
                m_transferClient.QueueTransfare(aRetMessage);
        }

        public void StartServer()
        {
            m_bServerRunning = true;
            listener.Start(m_nPort);
        }

        protected void SetupListener()
        {
            if (listener == null)
                listener = new Listener();

            if (DebugEvent != null)
                DebugEvent("Starting Game server");
            listener.Accepted += Listener_Accepted;
        }

        private void Listener_Accepted(object sender, SocketAcceptedEventArgs e)
        {
            listener.Stop();

            m_transferClient = new TransferClient(e.Accepted);
            m_transferClient.OutputFolder = m_sTempFolderPath;

            SetupEvents();
            m_transferClient.Run();
            m_bConnected = true;
            if (OnConnected != null)
                OnConnected(this);

            if (DebugEvent != null)
                TransferClient.DebugEvent += M_transferClient_DebugEvent;
        }

        private void M_transferClient_DebugEvent(string message)
        {
            this.DebugEvent(message);
        }

        protected abstract byte[] GetResponse(byte[] aReceivedMessage);

        public void ConnectToServer(int nPort, string sHost)
        {
            m_nPort = nPort;
            m_sHost = sHost;
            ConnectToServer();
        }

        protected void ConnectToServer()
        {
            m_transferClient = new TransferClient();
            m_transferClient.OutputFolder = m_sTempFolderPath;
            Console.WriteLine("at connect to server. Host = " + m_sHost + " , IP =  " + m_nPort);
            while (true)
            {
                if (m_transferClient != null && m_transferClient.Connected)
                {
                    break;
                }
                else if (m_transferClient == null)
                {
                    m_transferClient = new TransferClient();
                    m_transferClient.Connect(m_sHost, m_nPort, connectCallback);
                }
                else
                {
                    m_transferClient.Connect(m_sHost, m_nPort, connectCallback);
                }
                if (m_bCancelConnect)
                {
                    m_bCancelConnect = false;
                    return;
                }
                Thread.Sleep(100);
            }
            m_bConnected = true;
            if (OnConnected != null)
                OnConnected(this);
        }

        bool m_bServerRunning = false;
        void TransferClient_Disconnected(object sender, EventArgs e)
        {
            DeregisterEvents();

            m_transferClient = null;

            if (OnDisconnected != null)
                OnDisconnected(this);
            if (m_bServerRunning && listener != null)
                listener.Start(m_nPort);
        }

        private void connectCallback(object sender, string error)
        {
            if (error != null)
            {
                Console.WriteLine("at connect callback: " + error);
                if (m_transferClient != null)
                    m_transferClient.Close();
                m_transferClient = null;

                return;
            }
            SetupEvents();
            m_transferClient.OutputFolder = m_sTempFolderPath;
            m_transferClient.Run();
        }

        protected bool SendMessage(string sCommand, string sParam)
        {
            return SendMessage(sCommand, UtilFunctions.GetBytes(sParam));
        }

        protected bool SendMessage(string sCommand, bool bConnect = true)
        {
            return SendMessage(sCommand, new byte[] { }, bConnect);
        }

        protected bool SendMessage(string sCommand, byte[] aData, bool bConnect = true)
        {
            if (!IsConnected())
            {
                if (DebugEvent != null)
                    DebugEvent("Not connected");
                Console.WriteLine("Not connected to server");
                if (bConnect)
                {
                    ConnectToServer();
                }
                else
                {
                    return false;
                }
            }

            try
            {
                Console.WriteLine("Sending " + sCommand);
                m_transferClient.QueueTransfare(sCommand, aData);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public void Disconnect()
        {
            if (m_transferClient == null)
                return;

            m_transferClient.Close();
            m_transferClient = null;
            m_bConnected = false;
        }

        public void CancelConnect()
        {
            m_bCancelConnect = true;
        }

    }
}
