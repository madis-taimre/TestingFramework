﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PacketConnection
{
    public interface IConnectionElement<T>
    {
        T GetStructure();
    }
}
