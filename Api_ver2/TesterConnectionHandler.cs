﻿using System;
using System.Collections.Generic;
using PacketConnection;

namespace UnityTesting
{
    /// <summary>
    /// The connection handler, which establishes connection with the test manager at the program made with Unity.
    /// Handles sending and receveing message from the system under test (SUT).
    /// </summary>
    public class TesterConnectionHandler : ConnectionHandler
    {
        #region singleton
        private static volatile TesterConnectionHandler instance;
        private static object syncRoot = new Object();

        private const int SERVER_PORT_NR = 110;
        private const string HOST_NAME = "localhost";

        private const string TMP_FOLDER_PATH = "tmp\\mock\\";
        private TesterConnectionHandler() : base(TMP_FOLDER_PATH, SERVER_PORT_NR, "localhost") { }
        /// <summary>
        /// The Instance of the connection handler. One connection can be created.
        /// </summary>
        public static TesterConnectionHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new TesterConnectionHandler();
                    }
                }

                return instance;
            }
        }
        #endregion
        /// <summary>
        /// The delegate for view state events.
        /// </summary>
        /// <param name="lObjectStates">Object states received</param>
        public delegate void ObjectStatesEvent(List<AdapterObjectState> lObjectStates);
        /// <summary>
        /// Triggered when view states are received from the SUT.
        /// The method SendGetViewState should be called to get the view states.
        /// </summary>
        public event ObjectStatesEvent ObjectStatesReceived;

        const string GET_OBJECT_STATE = "get object state";
        const string TRIGGER_BUTTON = "trigger button";
        const string SET_LABEL_VALUE = "set label value";
        const string WAIT_FOR_OBJECT_APPEAR = "wait for object to appear";
        const string RUN_METHOD = "run method";

        void ProcessObjectStatesReceived(byte[] aObjectState)
        {
            List<AdapterObjectState> lObjectStates = UtilFunctions.ByteArrayToList<ObjectStateStructure, AdapterObjectState>(aObjectState, objStruct => new AdapterObjectState(objStruct));

            if (ObjectStatesReceived != null)
                ObjectStatesReceived(lObjectStates);
        }

        protected override byte[] GetResponse(byte[] aReceivedMessage, out bool bDeleteFile)
        {
            byte[] aValueReceived;
            string sMessage = UtilFunctions.GetMessageAndValue(aReceivedMessage, out aValueReceived);
            Console.WriteLine("Received message: " + sMessage);

            switch (sMessage.Trim('\0'))
            {
                case GET_OBJECT_STATE:
                    ProcessObjectStatesReceived(aValueReceived);
                    break;
            }
            bDeleteFile = true;
            return null;
        }
        /// <summary>
        /// Used to send a message to the SUT with a parameter, which is a view state.
        /// </summary>
        /// <param name="sMessage">Command, what the SUT should do. Can be a custom message, which is also handled in the SUT.</param>
        /// <param name="newViewState"></param>
        public void SendMessageToUnity(string sMessage, AdapterObjectState newObjectState)
        {
            if (newObjectState == null)
                SendMessage(sMessage);
            else
                SendMessage(sMessage, UtilFunctions.StructureToByteArray(newObjectState.GetStructure()));
        }
        
        public void SendMessageToUnity(string sMessage, string sParameters)
        {
            SendMessage(sMessage, sParameters);
        }

        /// <summary>
        /// Sends a message to the Unity asking for the state of an object in the SUT. 
        /// </summary>
        /// <param name="sObjectName">The object to be triggered.</param>
        public void SendGetObjectState (string sObjectName)
        {
            SendMessage(GET_OBJECT_STATE, sObjectName);
        }
        /// <summary>
        /// The method sends a message to the SUT to trigger the object with the same ID as the parameter's ID. This method can be used to trigger buttons in a menu or to click on objects in a game.
        /// </summary>
        /// <param name="sObjectName">The object to be triggered.</param>
        public void SendTriggerButton(string sObjectName)
        {
            SendMessage(TRIGGER_BUTTON, sObjectName);
        }
        /// <summary>
        /// This methods send a message to the SUT to set a new value to an object in the game with the same ID as the parameter's ID. The new value to be set is the Text property of the parameter.
        /// </summary>
        /// <param name="newObjState">The object to be changed.</param>
        public void SendSetLabelValue(AdapterObjectState newObjState)
        {
            SendMessageToUnity(SET_LABEL_VALUE, newObjState);
        }

        public void SendWaitForObjToAppear(string sObjName)
        {
            SendMessage(SET_LABEL_VALUE, sObjName);
        }

        /// <summary>
        /// This method can be used to send any data to the SUT's connection handler. It is possible to write additional code in the SUT's connection handler, which is able to use the data. The frameworks' connection handler is able to send messages of any length. When the message is very large it splits the message up to smaller pieces and sends them one by one. 
        /// </summary>
        /// <param name="sCommand">Command, what the SUT should do.</param>
        /// <param name="aData">Byte array of data to send.</param>
        public void SendData (string sCommand, byte[] aData)
        {
            SendMessage(sCommand, aData);
        }
    }
}
