﻿using PacketConnection;

namespace UnityTesting
{
    /// <summary>
    /// View state, that provides information about the state of an objet in the SUT.
    /// </summary>
    public class AdapterObjectState : ObjectState
    {
        public AdapterObjectState(ObjectStateStructure objectStateStruct) : base(objectStateStruct)
        { }

        public AdapterObjectState(string sName, byte nFlags, string sText, string sPosition, string sAnimation, float fNumericValue) : base(sName, 0, sText, sPosition, "", -1)
        { }
        /// <summary>
        /// Name of the object. Used to know, which object it is in the SUT.
        /// </summary>
        public string Name { get { return m_sName; } }
        /// <summary>
        /// The text attribute of the test object.
        /// </summary>
        public string Text { get { return m_sText; } }

        public string Animation {  get { return m_sAnimation; } }

        public float NumericValue { get { return m_fNumericValue; } }
    }
}
