﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PacketConnection
{
    /// <summary>
    /// Interface for connection elements. It is needed to get a structure of the object before it is sent.
    /// </summary>
    /// <typeparam name="T">The type of the corresponding structure.</typeparam>
    public interface IConnectionElement<T>
    {
        T GetStructure();
    }
}
