﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PacketConnection
{
    public static class UtilFunctions
    {
        const int MESSAGE_LENGTH = 48;
        /// <summary>
        /// Used to turn the structure to byte array for sending messages.
        /// </summary>
        /// <param name="str">Structure to be turned.</param>
        /// <returns>Byte array of the structure.</returns>
        public static byte[] StructureToByteArray(object str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        }
        /// <summary>
        /// Converts byte array into the specified structure.
        /// </summary>
        /// <typeparam name="T">The strucure type.</typeparam>
        /// <param name="arr">The byte array to be made into a structure.</param>
        /// <returns></returns>
        public static T ByteArrayToStructure<T>(byte[] arr)
        {
            T str = default(T);
            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(arr, 0, ptr, size);
            str = (T)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);
            return str;
        }
        /// <summary>
        /// Used by the ByteArrayToList method,
        /// </summary>
        public delegate ListElement StructureToListElement<ListElement, StructureElement>(StructureElement structure);
        /// <summary>
        /// Turns the byte array into a list of the specified elements. Used by getting the view state list from the data received.
        /// </summary>
        /// <typeparam name="StructureElement">Structure corresponding to the list object type.</typeparam>
        /// <typeparam name="ListElement">The object type.</typeparam>
        /// <param name="aData">The data to be turned into the list.</param>
        /// <param name="structToListElem">Method, that converts the structure to list element.</param>
        /// <returns></returns>
        public static List<ListElement> ByteArrayToList<StructureElement, ListElement>
            (byte[] aData, StructureToListElement<ListElement, StructureElement> structToListElem)
        {
            List<ListElement> lListElements = new List<ListElement>();
            int nSizeOfStructure = GetStructureSize<StructureElement>();
            int nNrOfStructures = aData.Length / nSizeOfStructure;
            byte[] aStructureInBytes = new byte[nSizeOfStructure];
            for (int i = 0; i < nNrOfStructures; i++)
            {
                Buffer.BlockCopy(aData, nSizeOfStructure * i, aStructureInBytes, 0, nSizeOfStructure);
                StructureElement structure = UtilFunctions.ByteArrayToStructure<StructureElement>(aStructureInBytes);
                lListElements.Add(structToListElem(structure));
            }
            return lListElements;
        }
        /// <summary>
        /// Returns the size of the structure.
        /// </summary>
        /// <typeparam name="T">Type of the structure.</typeparam>
        /// <returns>Size of the structure.</returns>
        public static int GetStructureSize<T>()
        {
            return Marshal.SizeOf(default(T));
        }
        /// <summary>
        /// Puts together the byte array to send with the connection.
        /// </summary>
        /// <param name="sMessage">Message to send.</param>
        /// <param name="aValue">Value to send.</param>
        /// <returns></returns>
        public static byte[] GetBytesToSend(string sMessage, byte[] aValue)
        {
            byte[] aMessageInBytes = Encoding.ASCII.GetBytes(sMessage);

            byte[] aByteToSend = new byte[MESSAGE_LENGTH + aValue.Length];
            Buffer.BlockCopy(aMessageInBytes, 0, aByteToSend, 0, aMessageInBytes.Length);
            Buffer.BlockCopy(aValue, 0, aByteToSend, MESSAGE_LENGTH, aValue.Length);
            return aByteToSend;
        }
        /// <summary>
        /// Returns the message received from the byte array and the value received (usually view state).
        /// </summary>
        /// <param name="aDataReceived">The received data from the connection.</param>
        /// <param name="aValue">The received value of the connection.</param>
        /// <returns>Message, which was received from the connetion.</returns>
        public static string GetMessageAndValue(byte[] aDataReceived, out byte[] aValue)
        {
            string sMessage = Encoding.ASCII.GetString(aDataReceived.Take(MESSAGE_LENGTH).ToArray());
            aValue = aDataReceived.Skip(MESSAGE_LENGTH).ToArray();
            return sMessage;
        }
        /// <summary>
        /// Returns byte array of the string.
        /// </summary>
        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        /// <summary>
        /// Used to get the string message from the bytes.
        /// </summary>
        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
