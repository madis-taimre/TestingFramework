﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnityMbtTesting
{
    /// <summary>
    /// The command received or to be sent to the tester.
    /// </summary>
    class TestCommand
    {
        /// <summary>
        /// The type of the command.
        /// </summary>
        public enum CommandType
        {
            GetViewState,
            Trigger,
            SetLabelValue
        }

        CommandType m_command;
        object m_param;

        public TestCommand (CommandType command, object param)
        {
            m_command = command;
            m_param = param;
        }
        /// <summary>
        /// Returns the command.
        /// </summary>
        public CommandType Command { get { return m_command; } }
        /// <summary>
        /// Returns any parameter attached to the command.
        /// </summary>
        public object Param { get { return m_param; } }
    }
}
