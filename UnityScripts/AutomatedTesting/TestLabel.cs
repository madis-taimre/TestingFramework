﻿using UnityEngine;
using System.Collections;
using UnityMbtTesting;
using UnityEngine.UI;
/// <summary>
/// Can be used to test InputField.
/// </summary>
public class TestLabel : TestObject {
    /// <summary>
    /// The InputField to be tested.
    /// </summary>
    public InputField Input;
    /// <summary>
    /// Method, which is used to set a new text value to the InputField.
    /// </summary>
    /// <param name="sText">New text value to be set.</param>
    public override void SetText(string sText)
    {
        Input.text = sText;
    }
}
