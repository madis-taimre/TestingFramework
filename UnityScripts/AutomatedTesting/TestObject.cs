﻿using UnityEngine;
using System.Collections;
using UnityMbtTesting;
using UnityEngine.UI;
using System.Text;
/// <summary>
/// Used mostly as a base class for different test objects.
/// </summary>
public class TestObject : MonoBehaviour {
    /// <summary>
    /// The ID should be specified and made unique. This is used to know, which element it is on the tester and Unity side.
    /// </summary>
    public int Id = 0;

    /// <summary>
    /// Default Text object handling, if the test object has Text attached.
    /// </summary>
    public Text Text;
    /// <summary>
    /// Default TextMesh object handling, if the test object has TextMesh attached.
    /// </summary>
    public TextMesh TextMesh;
    /// <summary>
    /// Default method for returning the parameter value of the test object.
    /// </summary>
    protected virtual string GetParameter ()
    {
        return Text != null ? Text.text : (TextMesh == null ? "" : TextMesh.text);
    }
    /// <summary>
    /// Returns the UnityViewState represenation of the test object.
    /// </summary>
    public virtual UnityViewState GetViewState ()
    {
        UnityViewState viewState = new UnityViewState(Id, GetParameter(), "");
        viewState.IsVisible = IsVisible;
        return viewState;
    }
    /// <summary>
    /// Used when anything is needed to be done to the test object before the view state can be received.
    /// </summary>
    public virtual bool PreGetViewState ()
    {
        return false;
    }
    /// <summary>
    /// Default method for checking whether the test object is visible.
    /// </summary>
    public virtual bool IsVisible 
    {
        get { return gameObject.activeInHierarchy; }
    }
    /// <summary>
    /// Default method for triggering the object.
    /// </summary>
    public virtual void Trigger ()
    {

    }
    /// <summary>
    /// Default method for setting the text of the object.
    /// </summary>
    /// <param name="sText">New text value to be set to the test object.</param>
    public virtual void SetText (string sText)
    {
        Text.text = sText;
        Debug.Log("At set text");
    }

}
