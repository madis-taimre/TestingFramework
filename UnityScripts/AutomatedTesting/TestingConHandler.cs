﻿using System;
using System.Collections;
using PacketConnection;

namespace UnityMbtTesting
{
    /// <summary>
    /// The connection handler, which creates the connection with the tester.
    /// </summary>
    public class TestingConHandler : ConnectionHandler
    {
        #region singleton
        private static volatile TestingConHandler instance;
        private static object syncRoot = new Object();
        const string TMP_FOLDER_PATH = "tmp\\";

        private const int SERVER_PORT_NR = 110;

        private TestingConHandler(): base (TMP_FOLDER_PATH, SERVER_PORT_NR, "localhost") { }
        /// <summary>
        /// Returns the instance of the connection handler. One connection allowed.
        /// </summary>
        public static TestingConHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new TestingConHandler();
                    }
                }

                return instance;
            }
        }
        #endregion

        public delegate byte[] GetViewStateHandler();
        public delegate void UnityCommand(string sId, string sParam);
        /// <summary>
        /// Event used to return the view state in bytes to the tester.
        /// </summary>
        public event GetViewStateHandler GetViewStateEvent;
        /// <summary>
        /// TriggerButton is triggered, when the trigger button message is received.
        /// SetLabelValue is triggered, when the set label value message is received.
        /// </summary>
        public event UnityCommand TriggerButton, SetLabelValue;

        /// <summary>
        /// Can be used to log message in Unity.
        /// </summary>
        public event DebugEventHandler TestingConDebug;

        const string GET_VIEW_STATE = "get view state";
        const string TRIGGER_BUTTON = "trigger button";
        const string SET_LABEL_VALUE = "set label value";
        /// <summary>
        /// Used to get the view state of the SUT.
        /// </summary>
        /// <returns></returns>
        private byte[] GetViewState ()
        {
            if (GetViewStateEvent != null)
                return GetViewStateEvent();
            if (TestingConDebug != null)
                TestingConDebug("AT SENDING NULL");
            return new byte[0];
        }
        /// <summary>
        /// Method for processing trigger button message.
        /// </summary>
        /// <param name="aValue">The test object to be triggered in bytes.</param>
        private void ProcessTriggerButton (byte[] aValue)
        {
            ViewStateStructure viewState = UtilFunctions.ByteArrayToStructure<ViewStateStructure>(aValue);
            TriggerButton(viewState.id, null);
        }
        /// <summary>
        /// Methd for processing set label value message.
        /// </summary>
        /// <param name="aValue">The test object and the new value in bytes.</param>
        private void ProcessSetLabel(byte[] aValue)
        {
            ViewStateStructure viewState = UtilFunctions.ByteArrayToStructure<ViewStateStructure>(aValue);
            SetLabelValue(viewState.id, viewState.text);
        }

        /// <summary>
        /// Method is used to process the received message and send a reply. No message is sent back when null is returned.
        /// </summary>
        /// <param name="receivedMessage">The message received from the tester.</param>
        /// <returns>The message to send back.</returns>
        protected override byte[] GetResponse(byte[] receivedMessage)
        {
            byte[] valueReceived;
            string sMessage = UtilFunctions.GetMessageAndValue(receivedMessage, out valueReceived);
            string sReturnMessage = "";
            byte[] valueToSend = new byte[] { };
            switch (sMessage.Trim('\0'))
            {
                case GET_VIEW_STATE:
                    sReturnMessage = GET_VIEW_STATE;
                    valueToSend = GetViewState();
                    break;
                case TRIGGER_BUTTON:
                    ProcessTriggerButton(valueReceived);
                    return null;
                case SET_LABEL_VALUE:
                    ProcessSetLabel(valueReceived);
                    break;
                default:
                    return null;
            }
            return UtilFunctions.GetBytesToSend(sReturnMessage, valueToSend);
        }
    }
}
