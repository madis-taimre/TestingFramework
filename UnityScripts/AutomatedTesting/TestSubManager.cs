﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Used to get all the TestObjects of the components in child GameObjects.
/// </summary>
public class TestSubManager : MonoBehaviour {
    /// <summary>
    /// Can be used, when the TestObjects are instantiate during gameplay. 
    /// </summary>
    public string TestObjectsId;

    private int m_nHighestId = 1;
    /// <summary>
    /// Max number of TestObjects can be specified to be returned. Default value is no limitation.
    /// </summary>
    public int MaxNrOfElements = -1;

    private TestObject[] m_aTestObjts;

    /// <summary>
    /// Gets the TestObjects attached to this object.
    /// </summary>
    public TestObject[] TestObjects
    {
        get
        {
            TestObject[] aAllTestObj = GetComponentsInChildren<TestObject>();
            int nNrOfElementsToTake = MaxNrOfElements == -1 ? aAllTestObj.Length : (MaxNrOfElements < aAllTestObj.Length ? MaxNrOfElements : aAllTestObj.Length);
            m_aTestObjts = new TestObject[nNrOfElementsToTake];
            for (int i = 0; i < nNrOfElementsToTake; i++)
            {
                m_aTestObjts[i] = aAllTestObj[i];
                if (aAllTestObj[i].Id == 0)
                {
                    m_aTestObjts[i].Id = int.Parse(TestObjectsId + m_nHighestId.ToString());
                    m_nHighestId++;
                }
                
            }

            return m_aTestObjts;
        }
    }

}
