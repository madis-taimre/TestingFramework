﻿using UnityEngine;
using System.Collections;
using PacketConnection;
using UnityMbtTesting;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
/// <summary>
/// The test manager for the SUT.
/// </summary>
public class TestingManager : Singleton<TestingManager> {

    //public int[] SearchedImagesIds;
    /// <summary>
    /// The queue of the commands to be processed.
    /// </summary>
    Queue<TestCommand> m_commandQueue = new Queue<TestCommand>();
    /// <summary>
    /// Contains the test objects of the scene.
    /// </summary>
    TestObjManager m_objManager;
    /// <summary>
    /// The connection handler.
    /// </summary>
    TestingConHandler m_conHandler;
    /// <summary>
    /// List of the Unity view states. Used to send back the view state to the SUT.
    /// </summary>
    private List<UnityViewState> m_aViewStates;
    /// <summary>
    /// Boolean to indicate whether to send the view state.
    /// </summary>
    bool m_bSendViewState = false;

    void Start ()
    {
        m_conHandler = TestingConHandler.Instance;
        m_conHandler.GetViewStateEvent += GetViewState;
        m_conHandler.TestingConDebug += Instance_TestingConDebug;
        m_conHandler.DebugEvent += Instance_TestingConDebug;
        m_conHandler.TriggerButton += Instance_TriggerButton;
        m_conHandler.SetLabelValue += Instance_SetLabelValue;
        m_conHandler.StartServer();

        //EuropeanaSearchManager.OnSearchedImagesProcessed += SearchedImagesProcessed;
        m_aViewStates = new List<UnityViewState>();
    }
    /// <summary>
    /// Called by the TestObjManager of the scene to update the object manager in the test manager (in this object).
    /// </summary>
    /// <param name="manager">The new test object manager in the scene.</param>
    public void SetObjManager (TestObjManager manager)
    {
        m_objManager = manager;
    }

    /// <summary>
    /// Used to get the test object with the ID.
    /// </summary>
    /// <param name="sId">The ID of the test object to be returned.</param>
    /// <returns>The test object.</returns>
    private TestObject GetTestObjWithId (string sId)
    {
        Debug.Log("At get test obj with id " + sId);
        return m_objManager.AllTestObjects.Find(p => p.Id == int.Parse(sId));
    }

    /// <summary>
    /// Used to set the label value. Method is called, when the set label value message is received.
    /// </summary>
    /// <param name="sId">The ID of the object to set the new value.</param>
    /// <param name="sParam">The new label value to be set.</param>
    private void Instance_SetLabelValue(string sId, string sParam)
    {
        m_commandQueue.Enqueue(new TestCommand(TestCommand.CommandType.SetLabelValue, new string[] { sId, sParam }));
    }
    /// <summary>
    /// Used to trigger the test object with the specified ID.
    /// </summary>
    /// <param name="sId">ID of the test object to be triggered.</param>
    /// <param name="sText">Not used.</param>
    private void Instance_TriggerButton(string sId, string sText)
    {
        m_commandQueue.Enqueue(new TestCommand(TestCommand.CommandType.Trigger, sId));
    }
    /// <summary>
    /// Used to log messages in Unity.
    /// </summary>
    /// <param name="message">Message to be logged.</param>
    private void Instance_TestingConDebug(string message)
    {
        Debug.Log(message);
    }
    /// <summary>
    /// Can be used to wait before triggering a button.
    /// </summary>
    private int m_nTriggerTimer = 0;

    void Update ()
    {
        m_nTriggerTimer--;
        if (m_nTriggerTimer > 0 || m_commandQueue.Count == 0)
            return;
        else
            m_nTriggerTimer = 0;
        TestCommand command = m_commandQueue.Dequeue();


        switch (command.Command)
        {
            case TestCommand.CommandType.GetViewState:
                m_aViewStates.Clear();
                StartCoroutine("RefreshViewState");
                break;
            case TestCommand.CommandType.SetLabelValue:
                string[] commandParams = (string[])command.Param;
                GetTestObjWithId(commandParams[0]).SetText(commandParams[1]);
                break;
            case TestCommand.CommandType.Trigger:
                GetTestObjWithId((string)command.Param).Trigger();
                Debug.Log("At triggering in update");
                //m_nTriggerTimer = 20;
                break;
        }
    }
    /// <summary>
    /// Used to refresh the list of the view states to be sent to the tester.
    /// </summary>
    /// <returns></returns>
    IEnumerator RefreshViewState ()
    {
        foreach (TestObject testObj in m_objManager.AllTestObjects)
        {
            if (testObj.PreGetViewState())
            {
                yield return new WaitForEndOfFrame();
                yield return new WaitForSeconds(0.1f);
            }

            m_aViewStates.Add(testObj.GetViewState());
        }
        m_bSendViewState = true;
    }
    /// <summary>
    /// Method used to get the view state of the SUT and send it to the tester.
    /// </summary>
    /// <returns></returns>
    protected byte[] GetViewState ()
    {
        m_commandQueue.Enqueue(new TestCommand(TestCommand.CommandType.GetViewState, null));

        while (m_bSendViewState == false)
        {
            Thread.Sleep(20);
        }
        m_bSendViewState = false;

        int nNrOfObj = m_aViewStates.Count;
        int nSizeOfStructure = UtilFunctions.GetStructureSize<ViewStateStructure>();
        byte[] aViewStates = new byte[nNrOfObj * nSizeOfStructure];
        for (int i = 0; i < nNrOfObj; i++)
            Buffer.BlockCopy(UtilFunctions.StructureToByteArray
                (m_aViewStates[i].GetStructure()), 0, aViewStates, i * nSizeOfStructure, nSizeOfStructure);

        return aViewStates;
    }
}
