﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// Used to attach to a Button being tested. Enabled overwriting triggering of the button.
/// </summary>
public class TestButton : TestObject {
    /// <summary>
    /// The button, which should be triggered.
    /// </summary>
    public Button ButtonToClick;
    /// <summary>
    /// Method for triggering the button.
    /// </summary>
    public override void Trigger()
    {

        if (IsVisible)
        {
            Button button = ButtonToClick == null ? GetComponent<Button>() : ButtonToClick;
            button.onClick.Invoke();
        }
    }

}
