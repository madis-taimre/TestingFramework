﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Needed to attach to a boolean indicator for which testing should be done. Used when the indicator is composed of two test objects like images.
/// </summary>
public class TestIndicator : TestObject {
    /// <summary>
    /// The GameObject, which is visible, when the indicator is set.
    /// </summary>
    public GameObject SetGameobject;
    /// <summary>
    /// The GameObject, which is visible, when the indicator is not set.
    /// </summary>
    public GameObject NotSetGameobject;
    /// <summary>
    /// Used to test whether the indicator is set.
    /// </summary>
    /// <returns>Returns whether the indicator is set or not.</returns>
    protected override string GetParameter()
    {
        return SetGameobject.activeInHierarchy ? "set" : NotSetGameobject.activeInHierarchy ? "not set" : "incorrect";
    }
}
