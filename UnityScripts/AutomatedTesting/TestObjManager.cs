﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// One TestObjManager should be per scene. All the test objects, which are needed to be tested,should be added to the TestObjects list.
/// Also contains the list of SubManagers, which can be used to get all TestObjects, which are in child GameObjects.
/// </summary>
public class TestObjManager : MonoBehaviour {
    /// <summary>
    /// The TestObjects in the scene.
    /// </summary>
    public List<TestObject> TestObjects;
    /// <summary>
    /// The SubManagers in the scene.
    /// </summary>
    public List<TestSubManager> SubManagers;
    
    void Start ()
    {
        TestingManager.Instance.SetObjManager(this);
    }
    /// <summary>
    /// Returns all the TestObjects specified in this object.
    /// </summary>
    public List<TestObject> AllTestObjects
    {
        get
        {
            List<TestObject> lAllTestObjects = new List<TestObject>();
            lAllTestObjects.AddRange(TestObjects);
            foreach (TestSubManager subManager in SubManagers)
            {
                lAllTestObjects.AddRange(subManager.TestObjects);
            }
            return lAllTestObjects;
        }
    }
}
