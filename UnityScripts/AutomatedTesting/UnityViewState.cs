﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PacketConnection;

namespace UnityMbtTesting
{
    /// <summary>
    /// The representation of the message to send and receive.
    /// </summary>
    public class UnityViewState : ViewState
    {
        public UnityViewState(ViewStateStructure viewStateStruct) : base (viewStateStruct)
        { }

        public UnityViewState(int nId, string sText, string sPosition) : base (nId, 0, sText, sPosition)
        { }
    }
}
